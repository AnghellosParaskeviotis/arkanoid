package game;

import java.awt.*;

public class Ball extends GameObject {
    static public final int WIDTH = 20;
    static public final int HEIGHT = 20;

    private int dx, dy;
    private Color color;

    public Ball(int x, int y, int dx, int dy, Color color) {
        super(x, y, Ball.WIDTH, Ball.HEIGHT);

        this.dx = dx;
        this.dy = dy;
        this.color = color;
    }

    public void moveIn(GameObject frame) {
        x += dx;
        y += dy;

        if (left() < frame.left()) {
            setLeft(frame.left());
        } else if (right() > frame.right()) {
            setRight(frame.right());
        }

        if (top() < frame.top()) {
            setTop(frame.top());
        } else if (bottom() > frame.bottom()) {
            setBottom(frame.bottom());
        }

        if (left() == frame.left()) {
            reflectHorizontal();
        } else if (right() == frame.right()) {
            reflectHorizontal();
        }

        if (top() == frame.top()) {
            reflectVertical();
        } else if (bottom() == frame.bottom()) {
            reflectVertical();
        }
// System.out.println(String.format("x: %d, y: %d", x, y));
    }

    public void paint(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, WIDTH, HEIGHT);
    }

    public void reflectHorizontal() {
        dx *= -1;
    }

    public void reflectVertical() {
        dy *= -1;
    }
}
