package game;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;

public class Arkanoid extends JFrame implements KeyListener {
    private final int WIDTH = 1024;
    private final int HEIGHT = 768;
    private final int GAME_DELAY = 10;
    private ArrayList<Ball> balls;
    private GameObject gameFrame;
    private Paddle paddle;


    public Arkanoid() {
        addKeyListener(this);
        setTitle("Arkanoid");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);

        setMinimumSize(new Dimension(WIDTH, HEIGHT));
        setMaximumSize(new Dimension(WIDTH, HEIGHT));

        setLocationRelativeTo(null);
        pack();

        setVisible(true);
        newGame();
    }

    public void newGame() {
        int paneHeight = getContentPane().getSize().height;

        gameFrame = new GameObject(0, HEIGHT - paneHeight, WIDTH, paneHeight);
        paddle = new Paddle(gameFrame.center_x() - Paddle.WIDTH / 2, gameFrame.bottom() - 3 * Paddle.HEIGHT, 15, Color.cyan);
        balls = new ArrayList<Ball>();
        balls.add(new Ball(100, 100, 1, 1, Color.cyan));
        balls.add(new Ball(100, 100, 2, 2, Color.pink));
        balls.add(new Ball(100, 100, 3, 3, Color.green));
    }

    public void run() {
        try {
            while (true) {
                update();

                Thread.sleep(GAME_DELAY); //ms
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void paint(Graphics g) {
        drawBackground(g);

        paddle.paint(g);
        for (Ball ball : balls) {
            ball.paint(g);
        }
    }

    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                paddle.moveLeftIn(gameFrame);
                forceRepaint();
                break;
            case KeyEvent.VK_RIGHT:
                paddle.moveRightIn(gameFrame);
                forceRepaint();
                break;
        }
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {
    }

    private void update() {
        for (Ball ball : balls) {
            ball.moveIn(gameFrame);
            if (ball.collide_with(paddle)) {
                ball.reflectVertical();
            }
        }
        forceRepaint();
    }

    private void drawBackground(Graphics g) {
        g.setColor(Color.darkGray);
        g.fillRect(0, 0, WIDTH, HEIGHT);
    }

    private void forceRepaint() {
        revalidate();
        repaint();
    }
}

