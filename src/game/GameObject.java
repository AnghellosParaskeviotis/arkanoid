package game;

public class GameObject {
    protected int x, y, w, h;

    public GameObject(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public int left() {
        return x;
    }

    public void setLeft(int x) {
        this.x = x;
    }

    public int right() {
        return (int) (x + w);
    }

    public void setRight(int x) {
        this.x = (int) (x - w);
    }

    public int center_x() {
        return (int) (x + w / 2);
    }

    public int top() {
        return y;
    }

    public void setTop(int y) {
        this.y = y;
    }

    public int bottom() {
        return (int) (y + h);
    }

    public void setBottom(int y) {
        this.y = (int) (y - h);
    }

    public int center_y() {
        return (int) (this.y + h / 2);
    }

    public boolean collide_with(GameObject other) {
        return (other.right() >= this.left() && other.left() <= this.right()) && (other.bottom() >= this.top() && other.top() <= this.bottom());
    }
}
