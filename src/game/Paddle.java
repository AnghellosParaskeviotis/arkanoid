package game;

import java.awt.*;

public class Paddle extends GameObject {
    static public final int WIDTH  = 7 * 20;
    static public final int HEIGHT = 20;

    private int dx;
    private Color color;

    public Paddle(int x, int y, int dx, Color color) {
        super(x, y, Paddle.WIDTH, Paddle.HEIGHT);

        this.dx = dx;
        this.color = color;
    }

    public void moveLeftIn(GameObject frame) {
        x -= dx;

        if (left() < frame.left()) {
            setLeft(frame.left());
        }
    }

    public void moveRightIn(GameObject frame) {
        x += dx;

        if (right() > frame.right()) {
            setRight(frame.right());
        }
    }

    public void paint(Graphics g) {
        g.setColor(color);
        g.fillRect(x, y, WIDTH, HEIGHT);
    }
}